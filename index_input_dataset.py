#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys
from python import check_config, save_load_variables, index_dataset

pkg_dir = os.path.dirname(os.path.realpath(sys.argv[0]))

parser = argparse.ArgumentParser(description='This tool aims at preparing python dictionaries of genomes, proteins and genomic positions')
parser.add_argument('-f', dest='fasta', type=str, help='path to input fasta', required=True)
parser.add_argument('--fasta_index', dest='fasta_index', type=str , help='path to fasta index')
parser.add_argument('-t', dest='taxa_dict', type=str, help='path to taxonomic table')
parser.add_argument('-g', dest='anno_file', type=str, help='path to file of genomic features')
parser.add_argument('-o', dest='output_dir', type=str, help='path to output directory where pickle variables will be saved', required=True)
args = parser.parse_args()

# Check config
config = check_config.check_args(args, pkg_dir)

# Index dataset
index_dataset.index_dataset(args.fasta_index, args.taxa_dict, args.anno_file, args.output_dir)
