#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
from python import save_load_variables, convert_taxa


def index_taxa(input_file, genome_dico):
    # read the taxa tabular file and assign taxomonmic properties to each genome_id in a dictionary
    with open(input_file, mode = 'r') as f:
        next(f)
        for line in f:
            line = line.strip('\n')
            fields = line.split('\t')
            key, assembly, taxid, lin, ranks = fields
            key = int(fields[0])
            genome_dico[key] = dict()
            genome_dico[key]['assembly'] = fields[1]
            genome_dico[key]['taxid'] = fields[2]
            genome_dico[key]['lineage'] = fields[3]
            genome_dico[key]['ranks'] = fields[4]
            taxa = fields[3].split('; ')
            genome_dico[key]['species'] = taxa[len(taxa)-1]
            ranks = fields[4].split('; ')
            genome_dico[key]['phylum'] = ''
            for i in range(0, len(ranks)):
                if ranks[i] == 'phylum':
                    genome_dico[key]['phylum'] = taxa[i]         
    f.close()
    

def index_seq(fasta_index, genome_dico):
    # add information about fasta sequences to each genome_dico[genome_id] dictionary
    with open(fasta_index, mode = 'r') as f:
        next(f)
        for line in f:
            line = line.strip('\n')
            fields = line.split('\t')
            key = int(fields[0])
            genome_dico[key]['fa_start'] = fields[1]
            genome_dico[key]['fa_end'] = fields[2]
            genome_dico[key]['n_proteins'] = fields[3]
    f.close()
    

def index_prot_and_pos(anno_file, protein_dict, position_dict, output_dir):
    data = pd.read_csv(anno_file, sep = "\t")
    position_dict = data[['genome', 'accession_id', 'position', 'protein']].set_index(['genome', 'accession_id', 'position']).T.to_dict()
    save_load_variables.save_to_pickle(position_dict, os.path.join(output_dir, 'position_dict.pickle'))
    protein_dict = data.set_index(['genome', 'protein']).T.to_dict()
    save_load_variables.save_to_pickle(protein_dict, os.path.join(output_dir, 'protein_dict.pickle'))

    
def index_dataset(fasta_index, taxa_dict, anno_file, output_dir):
    print(' * Indexing dataset ...')
    genome_dict = dict()
    protein_dict = dict()
    position_dict = dict()
    convert_taxa.append_taxa_dico_file(taxa_dict)
    index_taxa(taxa_dict, genome_dict)
    index_seq(fasta_index, genome_dict)
    save_load_variables.save_to_pickle(genome_dict, os.path.join(output_dir, 'genome_dict.pickle'))
    index_prot_and_pos(anno_file, protein_dict, position_dict, output_dir)
