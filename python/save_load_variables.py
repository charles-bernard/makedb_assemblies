#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import _pickle


def save_to_pickle(variable, output_file):
    f = open(output_file, mode = 'wb')
    _pickle.dump(variable, f)
    f.close()


def read_from_pickle(input_file):
    f = open(input_file, 'rb')
    variable = _pickle.load(f)
    f.close()
    return variable

