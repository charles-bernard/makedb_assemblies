#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from ete3 import *
ncbi = NCBITaxa()
import shutil
import tempfile
import os


def taxid_to_lineage(taxid):
    d = dict()
    if taxid:
        try:
            # get lineage as list of taxid
            lin = ncbi.get_lineage(taxid)
        except:
            print('Taxid %s not found in the local NCBITaxa database. Please update ete3 NCBITaxa database' % taxid)
            d['lin_names_str'] = ''
            d['lin_ranks_str'] = ''
            return d
    else:
        d['lin_names_str'] = ''
        d['lin_ranks_str'] = ''
        return d
    
    # convert each lineage taxid to the corresponding taxonomic name
    names = ncbi.get_taxid_translator(lin)
    # convert each lineage taxid to the corresponding name of the rank
    ranks = ncbi.get_rank(lin)
    # create strings for the lineage and the rank
    d['lin_names_str'] = names[lin[0]]
    d['lin_ranks_str'] = ranks[lin[0]]
    for i in range(1, len(lin)):
        d['lin_names_str'] = d['lin_names_str'] + '; ' + names[lin[i]]
        d['lin_ranks_str'] = d['lin_ranks_str'] + '; ' + ranks[lin[i]]
    return d
   

def append_taxa_dico_file(dico_filename):
    temp = tempfile.NamedTemporaryFile(mode='w+t', delete=False)
    with open(dico_filename, mode = 'r') as dico_file:
        header_line = next(dico_file).strip()
        temp.write(header_line + '\tLINEAGE\tRANKS\n')
        for line in dico_file:
            fields = line.split('\t')
            if len(fields) <= 3:
                taxid = fields[len(fields)-1].strip()
                lin = taxid_to_lineage(taxid)
                temp.write(line.strip() + '\t' + lin['lin_names_str'] + '\t' + lin['lin_ranks_str'] + '\n')
            else:
                temp.close()
                dico_file.close()
                return 0
    temp.close()
    dico_file.close()
    shutil.move(temp.name, dico_filename)
    