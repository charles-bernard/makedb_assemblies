#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import re
import subprocess
import sys


def check_tabular_file(nb_fields, filename):
    with open(filename, mode = 'r') as file:
        line = file.readline()
    file.close()
    n = len(re.findall(r'\t', line)) + 1
    if(n < nb_fields):
        sys.exit('File \'%s\' is not a tabular file with at least %d nb_fields.' % (filename, nb_fields))
        

def check_fasta(fasta):
    # check extension of the fasta file
    if not re.match(r'.*?\.fa(a|sta)?', fasta):
        sys.exit('File \'%s\' has an incorrect extension: \'.fa\', \'.faa\' or \'.fasta\' are expected for a fasta file.' % fasta)
    # check that the fasta file exists
    if not os.path.exists(fasta):
        sys.exit('Unable to locate \'%s\'. Please provide a correct path to the input fasta.' % fasta)
    
    # get first sequence header of the input fasta:
    with open(fasta, mode = 'r') as file:
        for line in file:
            if re.match(r'^>', line):
                break
    file.close()
    
    # get fasta format
    # fasta format 0: >SPECIED_ID_INTEGER|PROTEIN_ID_INTEGER
    # fasta format 1: >PROTEIN_ID .* [SPECIES]
    #                 requires taxa dictionary
    if re.match(r'>[A-Z0-9._]+.*?\[.*?\]', line):
        return(0)
    elif re.match(r'^>[A-Z0-9._]+__[0-9]+ ?.*$', line ):
        return(1)
    else:
        sys.exit('Headers of sequences in the input fasta file do not comply with the expected formats (see README)')

        
def check_taxa_dico(taxa_dico):
    if not os.path.exists(taxa_dico):
        sys.exit('Dictionary of taxa is required to comply with format 1 of the input fasta (see README)')
    check_tabular_file(2, taxa_dico)
    
    
def check_fasta_index(pkg_dir, fasta, fasta_index, fasta_format):
    if(fasta_index):
        if not os.path.exists(fasta_index):
            sys.exit('Unable to locate \'%s\'. Please provide a correct path to fasta index.' % fasta_index)
        check_tabular_file(4, fasta_index)
    else:
        fasta_index = os.path.splitext(fasta)[0] + '_index.tsv'
        make_index_script = os.path.join(pkg_dir, 'add-ons', 'input_formatting', 'create_seq_lib_index.awk')
        print(' * Generating fasta index ...')
        subprocess.call(['awk -v FORMAT=%s -f \'%s\' \'%s\' > \'%s\'' % (fasta_format, make_index_script, fasta, fasta_index)], shell = True)
    return(fasta_index)


def check_output_dir(output_dir):
    output_dir = os.path.abspath(output_dir)
    # Create output directory if parent directory exists
    if not os.path.exists(output_dir):
        parent_dir = os.path.dirname(output_dir)
        if os.path.exists(parent_dir):
            os.mkdir(output_dir)
        else:
            sys.exit('Unable to locate \'%s\'. Please provide a correct path to the output directory' % parent_dir)
    else:
        if os.listdir(output_dir):
            sys.exit('The directory \'%s\' is not empty. Please free it or provide another output directory' % output_dir)
            

def check_dict_dir(dico_dir):
    dico_dir = os.path.abspath(dico_dir)
    if not os.path.exists(dico_dir):
        sys.exit('Unable to locate \'%s\'. Please provide a correct path to the dict directory' % dico_dir)
    if not os.path.exists(os.path.join(dico_dir, 'genome_dict.pickle')):
        sys.exit('Unable to find the file \'genome_dict.pickle\' within the dict directory' % dico_dir)
    if not os.path.exists(os.path.join(dico_dir, 'position_dict.pickle')):
        pos_dir = os.path.join(dico_dir, 'genome_attributes', 'positions')
        if not os.path.exists(pos_dir):
            sys.exit('Unable to find either of the \'genome_attributes/positions\' dir or of the \'position_dict.pickle\' file within \'%s\'' % dico_dir)
        elif not os.listdir(pos_dir):
            sys.exit('\'%s\' directory is empty' % pos_dir)
    if not os.path.exists(os.path.join(dico_dir, 'protein_dict.pickle')):
        pos_dir = os.path.join(dico_dir, 'genome_attributes', 'proteins')
        if not os.path.exists(pos_dir):
            sys.exit('Unable to find either of the \'genome_attributes/proteins\' dir or of the \'protein_dict.pickle\' file within \'%s\'' % dico_dir)
        elif not os.listdir(pos_dir):
            sys.exit('\'%s\' directory is empty' % pos_dir)
        return(1)
    return(0)
        
        
def check_args(args, pkg_dir):
    print('* Checking config ...')
    
    config = dict()
    
    # check output directory
    check_output_dir(args.output_dir)
    
    # check fasta
    config['fasta_format'] = check_fasta(args.fasta)
    
    # check taxa_dico
    if(config['fasta_format'] == 1): 
        check_taxa_dico(args.taxa_dict)
        
    # check fasta index
    if(config['fasta_format'] == 1): # This condition is temporary since the generation of index should eventually work for any format
        config['fasta_index'] = check_fasta_index(pkg_dir, args.fasta, args.fasta_index, config['fasta_format'])
    
    return config
            