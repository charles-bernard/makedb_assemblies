# makedb_assemblies

Use a NCBI Assembly query search and create a dataset of protein sequences with related taxonomic and genomic metadata as python dictionaries

## Download the protein sequences and the feature tables of the assemblies

Go on the NCBI assembly database https://www.ncbi.nlm.nih.gov/assembly/ and type your query search. 

For instance, if you seek all the latest versions of assemblies of complete bacterial genomes, type:

```
Bacteria[Filter] AND Latest[Filter] AND "Complete Genome"[Filter] 
```

Then, click the button "Download Assemblies" and download:
- the "Protein FASTA (.faa)" file with "Genbank" selected as source database
- the "Feature table (.txt)" file with "Genbank" selected as source database

## Copy these two files (.tar) inside an empty directory

For instance:

```
mkdir ~/complete_genomes_of_bacteria
cp ~/Download/genome_assemblies_prot_fasta.tar ~/complete_genomes_of_bacteria
cp ~/Download/genome_assemblies_features.tar ~/complete_genomes_of_bacteria
```

## Clone this repository

For instance:

```
cd ~
git clone https://gitlab.com/charles-bernard/makedb_assemblies.git
```


## Prepare the indexation of the dataset

Run the prepare_input_dataset.sh script shell with the path to the repository containing the assemblies as argument 

```
bash ~/makedb_assemblies/prepare_input_dataset.sh ~/complete_genomes_of_bacteria
```

## Run the indexation of the dataset

Dependencies:

- python package ```_pickle```
- python package ```pandas```
- python package ```ete3``` (with NCBI tax table updated beforehand ; see http://etetoolkit.org/docs/latest/tutorial/tutorial_ncbitaxonomy.html#upgrading-the-local-database)

Usage:

```
usage: index_input_dataset.py [-h] -f FASTA [--fasta_index FASTA_INDEX]
                              [-t TAXA_DICT] [-g ANNO_FILE] -o OUTPUT_DIR

This tool aims at preparing python dictionaries of genomes, proteins and 
genomic positions

optional arguments:
  -h, --help            show this help message and exit
  -f FASTA              path to input fasta
  --fasta_index FASTA_INDEX
                        path to fasta index 
  -t TAXA_DICT          path to taxonomic table
  -g ANNO_FILE          path to file of genomic features
  -o OUTPUT_DIR         path to output directory where pickle variables will
                        be saved
```

For instance

```
python ~/makedb_assemblies/index_input_dataset.py \
-f ~/complete_genomes_of_bacteria/sequences_library.fasta \
--fasta_index ~/complete_genomes_of_bacteria/sequences_library_index.tsv \
-t ~/complete_genomes_of_bacteria/taxa_dico.tsv \
-g ~/complete_genomes_of_bacteria/annotations.tsv \
-o ~/complete_genomes_of_bacteria/python_dictionaries
```

## Usage of the dictionaries

Open a python console by typing ```python``` inside a terminal.

Then, for instance:

```python
import _pickle

def read_from_pickle(input_file):
    f = open(input_file, 'rb')
    variable = _pickle.load(f)
    f.close()
    return variable

genome_dict = read_from_pickle('/home/user/complete_genomes_of_bacteria/python_dictionaries/genome_dict.pickle')
position_dict = read_from_pickle('/home/user/complete_genomes_of_bacteria/python_dictionaries/position_dict.pickle')
protein_dict = read_from_pickle('/home/user/complete_genomes_of_bacteria/python_dictionaries/protein_dict.pickle')
```

### Genome Dictionary

```python
genome_id = 1
print genome_dict[genome_id]
```

```
{ 'assembly': 'GCA_000005825.2', 
  'taxid': '398511', 
  'lineage': 'root; cellular organisms; Bacteria; Terrabacteria group; Firmicutes; Bacilli; Bacillales; Bacillaceae; Bacillus; Bacillus pseudofirmus; Bacillus pseudofirmus OF4', 
  'ranks': 'no rank; no rank; superkingdom; no rank; phylum; class; order; family; genus; species; no rank', 
  'species': 'Bacillus pseudofirmus OF4', 
  'phylum': 'Firmicutes', 
  'fa_start': '1', 
  'fa_end': '21429', 
  'n_proteins': '4335' }
```

### Position Dictionary

```python
genome_id = 1
accession_id = 1 # 1 id per molecule (chromosome, plasmid etc...)
position = 1
print position_dict[(genome_id, accession_id, position_id)]
```

```
{ 'protein': 'ADC49649.1' }

```

### Protein Dictionary

```python 
genome_id = 1
protein_id = 'ADC49649.1'
print protein_dict[(genome_id, protein_id)]
```

```
{ 'accession_id': 1,
  'position': 1, 
  'start': 816, 
  'end': 2168, 
  'strand': '+', 
  'seq_type': 'chromosome', 
  'description': 'chromosomal replication initiation protein',
  'genomic_accession': 'CP001878.2' }
```
