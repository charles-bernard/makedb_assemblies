#!/bin/bash

#####################################################
# READ ARGUMENT
#####################################################
NCBI_DIR=$1;

if [ ! "$NCBI_DIR" ]; then
	echo "The path to the NCBI assembly download directory is required as argument";
	exit 1
elif [ ! -d "$NCBI_DIR" ]; then
	echo "Unable to locate $NCBI_DIR. Please provide a correct path to NCBI assembly download directory";
	exit 2
elif [ -z "$(ls -A "$NCBI_DIR")" ]; then
	echo "The directory $NCBI_DIR is empty"
	exit 3
fi
cd "$NCBI_DIR";

#####################################################
# EXTRACT COMPRESSED FILES
#####################################################
# Untar the fasta and annotation directories
for F in *.tar; do tar -xvf "$F"; done

# # Identify genome directory
GENOME_DIR=`ls | grep 'ncbi-genomes'`;
cd "$GENOME_DIR";

# Ungz all fasta and files
echo "* Unzipping all files - it may take a while..."
for F in *.gz; do gzip -d "$F"; done

# #####################################################
# # ASSIGN KEY SPECIES_ID TO EACH ASSEMBLY
# #####################################################
# List unique assembly ids and assign them a unique species integer id
LIST_ASSEMBLIES="$NCBI_DIR"/list_of_assemblies.txt;
ls | grep '^GC' > tmp.txt;

awk -F "_" '
BEGIN {
	print "SPECIES_ID\tASSEMBLY_ID";
	k = 1;
}
{ 
	id = $1 "_" $2; 
	if(!visited[id]) { 
		print k "\t" id; 
		visited[id] = 1;
		k++;
	}
}' tmp.txt > "$LIST_ASSEMBLIES";
rm tmp.txt;

# Count total number of assemblies
N=`awk 'END { print NR-1; }' "$LIST_ASSEMBLIES"`;

#####################################################
# DEFINE OUTPUT FILES
#####################################################
# - Pooled fasta
P_FASTA="$NCBI_DIR"/sequences_library.fasta;
> "$P_FASTA";
# - Index of fasta (start and end lines for each species)
FASTA_INDEX="$NCBI_DIR"/sequences_library_index.tsv;
printf "SPECIES_ID\tSTART_LINE\tEND_LINE\tNB_PROTEINS\n" > "$FASTA_INDEX";
IDX=1;
# - Dictionary of taxa
TAXA_DICO="$NCBI_DIR"/taxa_dico.tsv;
printf "SPECIES_ID\tASSEMBLY_ID\tTAX_ID\n" > "$TAXA_DICO";
EFFECTIVE_SPECIES_ID=0;
# - Pooled annotation file
ANNO_FILE="$NCBI_DIR"/annotations.tsv;
printf  "genome\taccession_id\tposition\tprotein\tstart\tend\tstrand\tmolecule\tdescription\tgenomic_accession\n" > "$ANNO_FILE";

#####################################################
# CREATE FASTA OUTPUT LIBRARY AND GENOME ANNOTATION FILE
#####################################################
CURR_SPECIES_ID=0;
EFFECTIVE_SPECIES_ID=0;
# Format input files
for (( I=0; I<$N; I++ )); do
	CURR_SPECIES_ID=$((I+1));
	CURR_ASSEMBLY_ID=`awk -v ID=$I -F "\t" '$1 == ID { print $2; }' "$LIST_ASSEMBLIES"`;
	echo "Formating assembly $CURR_SPECIES_ID / $N: $CURR_ASSEMBLY_ID";

	CURR_FASTA=`ls | grep "$CURR_ASSEMBLY_ID"*.faa`;

	if [ ! "$CURR_FASTA" ]; then
		echo "   Unable to locate fasta of this assembly";
	else
		EFFECTIVE_SPECIES_ID=$((EFFECTIVE_SPECIES_ID+1));

		# Format fasta
		# Keep only protein id, followed by the curr_species_id (sep = "__")
		sed '\\>\ { s/ .*$/__'"$EFFECTIVE_SPECIES_ID"'/g }' "$CURR_FASTA" >> "$P_FASTA";

		# Append index of the sequence library
		NB_LINES=`awk 'END { print NR; }' "$CURR_FASTA"`;
		NB_PROTS=`grep -c '>' "$CURR_FASTA"`;
		printf "$EFFECTIVE_SPECIES_ID\t$IDX\t$((IDX+NB_LINES-1))\t$NB_PROTS\n" >> "$FASTA_INDEX";
		IDX=$((IDX+NB_LINES));

		# format feature table and append to genome annotation file
		CURR_TBL=`ls | grep "$CURR_ASSEMBLY_ID"*feature_table.txt`;
		if [ "$CURR_TBL" ]; then
			awk -v genome="$EFFECTIVE_SPECIES_ID" '
			BEGIN { 
				FS = "\t"; 
				genomic_accession = ""; 
				k = 0; 
			}
			$1 $2 == "CDSwith_protein" { 
				if($7 != genomic_accession) { 
					pos = 0; 
					genomic_accession = $7; 
					k++; 
				}
				pos++; 
				printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", genome, k, pos, $11, $8, $9, $10, $5, $14, $7);
			}' "$CURR_TBL" >> "$ANNO_FILE";
		fi

		# Fetch corresponding tax_id
		CURR_TAXID=`esearch -db assembly -query $CURR_ASSEMBLY_ID | esummary | xtract -pattern DocumentSummary -def "NA" -element Taxid`
		printf "$EFFECTIVE_SPECIES_ID\t$CURR_ASSEMBLY_ID\t$CURR_TAXID\n" >> "$TAXA_DICO";
	fi
done

cd "$NCBI_DIR";
rm "$LIST_ASSEMBLIES";
#rm -r "$GENOME_DIR";
